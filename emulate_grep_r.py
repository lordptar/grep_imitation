#!/usr/bin/python

import sys
import os
import re
from glob import glob

colour_folders = '\033[95m'
colour_end = '\033[0m'
colour_words = '\033[91m'


directory = sys.argv[2:]
if not directory:
    directory = "."
print(colour_words + "directories: " + colour_end + str(directory))


try:
    raw_pattern = sys.argv[1]
except IndexError as indexerr:
    print("Usage: emulate_grep PATTERN [FILE]... ")
    sys.exit()
print("")
print(colour_words + "pattern: " + colour_end + raw_pattern)
com_pattern = re.compile(raw_pattern)


def file_list(directory):
    draft_file_list = [y for x in os.walk(directory) for y in glob(os.path.join(x[0], "*"))]
    clean_file_list = [each_file for each_file in draft_file_list if os.path.isfile(each_file)]
    return(clean_file_list)


def open_file(file_name, multiple=None):
    try:
        with open(file_name) as file_data:
            for line in file_data:
                line = line.strip()
                if com_pattern.search(line):
                    line = re.sub('(' + raw_pattern + ')', r'\033[91m\1\033[0m', line)
                    if multiple:
                        print(colour_folders + file_name + ":" + colour_end + line)
                    else:
                        print(line)
    except IOError as ioerr:
            print('IO errors: ' + str(ioerr))
            return(None)

print("")

for each_arg in directory:
    if os.path.isfile(each_arg):
        open_file(each_arg, "yes")
    else:
        for each_file in file_list(each_arg):
            open_file(each_file, "yes")
