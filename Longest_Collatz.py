#!/usr/bin/python3

import sys
from time import strftime, time, localtime, gmtime

start_time = time()
print()
print("The start time of the program:  " + strftime("%H:%M:%S", localtime(start_time)))

try:
    if (sys.argv[2]):
        print("Too many arguments")
        sys.exit()
except IndexError as indexerr:
    pass

try:
    n = int(sys.argv[1])
except ValueError as valerr:
    print("the provided argument is not an integer - " + sys.argv[1])
    sys.exit()
except IndexError as indexerr:
    print("no integer had been provided")
    sys.exit()

term_list = []


def is_even(i):
    return((i % 2) == 0)


def collatz_conjecture(n):
    while (n > 1):
        global terms
        terms += 1
        if is_even(n):
            n = n // 2
        else:
            n = 3 * n + 1

for i in range(1, n):
    terms = 1
    collatz_conjecture(i)
    term_list.append(terms)

the_biggest = sorted(set(term_list), reverse=True)[0]
index = term_list.index(the_biggest)
print()
print("The longest chain is:")
print("the starting number: " + str(index + 1) + " terms: " + str(the_biggest))
print()
end_time = time()
print("The end time of the program: " + strftime("%H:%M:%S", localtime(end_time)))
duration = end_time - start_time
print("The duration of the program: " + strftime("%H:%M:%S", gmtime(duration)))
print("The duration of the program in seconds: " + str(duration))
print("The End")
