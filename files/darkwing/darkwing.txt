Darkwing Duck:
 
Darkwing Duck is an animated action-adventure comedy television series produced by The Walt Disney Company that first ran from 1991 to 1992 on both the syndicated programming block The Disney Afternoon and Saturday mornings on ABC.[1] It featured the eponymous anthropomorphic duck superhero whose alter ego is suburban father Drake Mallard. It was a spin-off of DuckTales.

Premise[edit]
Darkwing Duck tells the adventures of the titular superhero, aided by his sidekick and pilot Launchpad McQuack. In his secret identity of Drake Mallard (a parody of Kent Allard, the alter ego of the Shadow), he lives in an unassuming suburban house with his adopted daughter Gosalyn, next door to the bafflingly dim-witted Muddlefoot family. Darkwing struggles to balance his egotistical craving for fame and attention against his desire to be a good father to Gosalyn and help do good in St. Canard. Most episodes put these two aspects of Darkwing's character in direct conflict, though Darkwing's better nature usually prevails.[2]

The show was the first Disney Afternoon series to emphasize action rather than adventure, with Darkwing routinely engaging in slapstick battles with both supervillains and street criminals. While conflict with villains was routine in earlier Disney Afternoon shows, actual fight scenes were relatively rare.

Darkwing Duck was also the first Disney Afternoon property that was produced completely as a genre parody. Prior shows would contain elements of parody in certain episodes, but would otherwise be straight-faced adventure concepts, this in the tradition of Carl Barks' work in the Disney comics. By contrast, every episode of Darkwing Duck is laden with references to superhero, pulp adventure, or super-spy fiction. Darkwing Duck himself is a satirical character. His costume, gas gun and flashy introductions are all reminiscent of pulp heroes and Golden Age superheroes such as The Shadow, The Sandman, Doc Savage, Batman, The Green Hornet and the Julius Schwartz Flash, as well as The Lone Ranger and Zorro. The fictional city of St. Canard is a direct parody of Gotham City.


